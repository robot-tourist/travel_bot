from datetime import datetime, timedelta
from openpyxl.styles import PatternFill, Color
from openpyxl import Workbook
from openpyxl.styles import Font
import json
import requests

with open("constants.json") as js:
    CONSTANTS = json.load(js)


def compile_date_range():
    """Возвращает дату, месяц и день в формате ГГГГ-ММ-ДД c разницей в три недели"""
    now = datetime.utcnow() + timedelta(days=2)
    now_str = now.strftime("%Y-%m-%d")
    three_weeks_later = now + timedelta(weeks=3)
    return now_str, three_weeks_later.strftime("%Y-%m-%d")


def convert_into_xlsx(ws, base_idx, tours):
    meta_info = {"departure": tours["departure"], "direction": tours["direction"]}
    if "data" not in tours or not tours["data"]:
        return -1
    return_val = 1
    for i, tour in enumerate(tours["data"]):
        tour.update(meta_info)
        for j, key in enumerate(CONSTANTS["keys"]):
            ws.cell(column=j+1, row=base_idx + i, value=tour[key]).fill = \
                PatternFill(start_color=tours["bgColor"], end_color=tours["bgColor"], fill_type="solid")
        return_val = i
    return return_val


def parse_json_response(json_responses: list, from_date: str, to_date: str):
    wb = Workbook()
    ws = wb.active
    for i, el in enumerate(CONSTANTS["keys"]):
        ws.cell(column=i+1, row=1, value=el)
    row = ws.row_dimensions[1]
    row.font = Font(bold=True,
                    italic=False,
                    vertAlign=None,
                    underline='none',
                    strike=False,
                    color='FF000000')
    base_idx = 2
    for response in json_responses:
        if "data" not in response:
            continue
        new_idx = convert_into_xlsx(ws, base_idx, response)
        base_idx += new_idx + 1
    for column_cells in ws.columns:
        length = max(len(str(cell.value)) for cell in column_cells)
        ws.column_dimensions[column_cells[0].column_letter].width = length
    filename = f'{from_date}_{to_date}.xlsx'
    wb.save(filename)
    wb.close()


def get_tours():
    try:
        date_from, date_to = compile_date_range()
        responses = []
        for dep_key, dep_values in CONSTANTS["departureCities"].items():
            for dest_key, dest_values in CONSTANTS["destinationCountries"].items():
                bgcolour = CONSTANTS["bgColors"]
                print(f"post_request: {dep_values}, {dest_values}, {date_from}, {date_to}")
                request_link = CONSTANTS["request_template"].format(dest_key, dep_key, date_from, date_to)
                response = requests.get(request_link).text
                json_response = json.loads(response)
                json_response.update({"departure": dep_values, "direction": dest_values, "bgColor": bgcolour[dest_key]})
                responses.append(json_response)
        parse_json_response(responses, date_from, date_to)
        return f'{date_from}_{date_to}.xlsx'
    except Exception as e:
        raise
        print(e)
        return
