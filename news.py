import urllib.request
from bs4 import BeautifulSoup
import requests
from copy import deepcopy
from tours import CONSTANTS


def tiny_url(url):
    apiurl = "http://tinyurl.com/api-create.php?url="
    tinyurl = urllib.request.urlopen(apiurl + url).read()
    return tinyurl.decode("utf-8")


def get_tonkosti():
    session = requests.session()
    response = session.get("https://tonkosti.ru/Новости_туризма", headers=CONSTANTS["headers"])
    soup = BeautifulSoup(response.text, 'html.parser')
    msgs = []
    for element in soup.find_all(['h3'], class_="article-trailer__title"):
        for news in element.findAll('a', limit=1):
            msg = news['title'] + "\n" + tiny_url('https://tonkosti.ru/' + news['href'])
            msgs.append(msg)
    return "\n\n-------\n".join(msgs)


def get_ator_news():
    short_news, description_news, dates, links = [], [], [], []
    session = requests.session()
    response = session.get("https://www.atorus.ru/news/press-centre.html", headers=CONSTANTS["headers"])
    soup = BeautifulSoup(response.text, 'html.parser')
    for element in soup.find_all('li'):
        if element.find('div', class_='small'):
            short_news.append("Короткая новость:%s" % element.find('a', class_="name").text.strip())
            description_news.append("Описание новости:%s" % element.find('div').text.strip())
            dates.append("Дата публикации:%s" % element.find('p', class_='date').text.strip())
            links.append(tiny_url(element.find('a').get('href')))

    msgs = []
    msg = ""
    for data in zip(short_news, description_news, dates, links):
        if len(msg) + len("\n".join(data)) + len('\n\n-------\n') >= 4090:
            msgs.append(deepcopy(msg))
            msg = ""
        msg += "\n".join(data)
        msg += '\n\n-------\n'
    msgs.append(msg)
    return msgs
