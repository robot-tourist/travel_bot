import telebot
from time import gmtime, strftime
from xlsx2html import xlsx2html
import requests
from tours import get_tours, CONSTANTS
from news import get_tonkosti, get_ator_news
import os

bot = telebot.TeleBot(CONSTANTS["token"])


def send_files(message, filename):
    xlsx2html(filename, filename.replace("xlsx", "html"))
    f = open(filename, "rb")
    f_html = open(filename.replace("xlsx", "html"), "rb")
    bot.send_message(message.chat.id, 'Вот ваши файлы в двух форматах!')
    bot.send_document(message.chat.id, f)
    os.remove(filename)
    bot.send_document(message.chat.id, f_html)
    os.remove(filename.replace("xlsx", "html"))


@bot.message_handler(commands=['start'])
def start_message(message):
    keyboard1 = telebot.types.ReplyKeyboardMarkup()
    keyboard1.row("1", "2", "3", "4", "5", "6")
    bot.send_message(message.chat.id, 'Привет, ты попал в travel_bot', reply_markup=keyboard1)
    bot.send_sticker(message.chat.id, 'CAADAgADZgkAAnlc4gmfCor5YbYYRAI')
    bot.send_message(message.chat.id,
                     """Выберите, что хотите скачать:
                     1) Предложения по заранее определенным маршрутам на выходные, праздники (список направлений указаны на Confluence)
                     2) Новости о туризме (Тонкости + АТОР)
                     3) Дешевые туры (тревелата)
                     4) Дешевые билеты заграницу (Билеты сгруппированы по странам, упорядочены по минимальной цене билета в страну)
                     5) Дешевые билеты по России (Билеты упорядочены по цене билета)""")


@bot.message_handler(content_types=['text'])
def send_text(message):
    bot.send_message(message.chat.id, 'Подождите, добыча данных займет около трех минут...')
    try:
        if message.text == "2":
            msgs = [get_tonkosti()] + get_ator_news()
            for msg in msgs:
                bot.send_message(message.chat.id, msg)
        elif message.text == "3":
            filename = get_tours()
            send_files(message, filename)
        else:
            if message.text == '1':
                resp = requests.get("http://andrwvrn.pythonanywhere.com/download_tickets")
                filename = f'{strftime("directons_%Y-%m-%d_%H:%M", gmtime())}.xlsx'
            elif message.text == "4":
                resp = requests.get("http://andrwvrn.pythonanywhere.com/download_cheap_abroad")
                filename = f'{strftime("cheap_abroad_%Y-%m-%d_%H:%M", gmtime())}.xlsx'
            elif message.text == "5":
                resp = requests.get("http://andrwvrn.pythonanywhere.com/download_cheap_rus")
                filename = f'{strftime("cheap_rus_%Y-%m-%d_%H:%M", gmtime())}.xlsx'
            else:
                filename = ""
                resp = 0
            if not filename:
                bot.send_message(message.chat.id, "Что-то пошло не так! Я не понял команду")
            else:
                with open(filename, 'wb') as f:
                    f.write(resp.content)
                send_files(message, filename)
    except Exception as e:
        bot.send_message(message.chat.id, f'Ошибка вышла: {e}')


if __name__ == '__main__':
    bot.polling()
